package com.teliapp.ibox;

import java.util.List;

import com.teliapp.iview.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
 
public class FileArrayAdapter extends ArrayAdapter<String> {
	private final Context context;
	private final List<String> values;
	private final int dirIndex;
 
	public FileArrayAdapter(Context context, List<String> values, int dirIndex) {
				
		super(context, R.layout.list_view, values);
		this.context = context;
		this.values = values;
		this.dirIndex = dirIndex;
	}
 
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context
			.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
 
		String file = values.get(position);
		
		View rowView = inflater.inflate(R.layout.list_view, parent, false);
		TextView textView = (TextView) rowView.findViewById(R.id.label);
		ImageView imageView = (ImageView) rowView.findViewById(R.id.logo);
		textView.setText(file);
  
		if(dirIndex == -1) {
			setIcon(file, imageView, position);
			
		} else {
			if(position <= dirIndex) {
				imageView.setImageResource(R.drawable.dir_icon);
			} else {
				setIcon(file, imageView, position);
			}
		}
 
		return rowView;
	}
	
	public void setIcon(String file, ImageView imageView, int position) {
		
		file = file.toLowerCase();
		
		if(file.endsWith(".pdf")) {
			imageView.setImageResource(R.drawable.pdf_icon);
		} else if(file.endsWith(".pages")) {
			imageView.setImageResource(R.drawable.pages_icon);
		} else if(file.endsWith(".numbers")) {
			imageView.setImageResource(R.drawable.numbers_icon);
		} else if(file.endsWith(".key")) {
			imageView.setImageResource(R.drawable.keynote_icon);
		} else if(file.endsWith(".txt")) {
			imageView.setImageResource(R.drawable.text_icon);
		} else if(file.endsWith(".rtf")) {
			imageView.setImageResource(R.drawable.rich_text_icon);
		} else if(file.endsWith(".mp3") || file.endsWith(".m4a") 
				|| file.endsWith(".aiff") || file.endsWith(".wav") ) {
			imageView.setImageResource(R.drawable.audio_icon);
		} else if(file.endsWith(".mov") || file.endsWith(".mp4") 
				|| file.endsWith(".m4v") || file.endsWith(".avi")
				|| file.endsWith(".mpg") || file.endsWith(".wmv") || file.endsWith(".flv")) {
			imageView.setImageResource(R.drawable.video_icon);
		} else if(file.endsWith(".doc") || file.endsWith(".docx")) {
			imageView.setImageResource(R.drawable.ms_doc_icon);
		} else if(file.endsWith(".xls") || file.endsWith(".xlsx")) {
			imageView.setImageResource(R.drawable.excel_icon);
		} else if(file.endsWith(".htm") || file.endsWith(".html")) {
			imageView.setImageResource(R.drawable.web_icon);
		} else if(file.endsWith(".ppt") || file.endsWith(".pptx")) {
			imageView.setImageResource(R.drawable.powerpoint_icon);
		} else if(file.endsWith(".jpg") || file.endsWith(".jpeg")
				|| file.endsWith(".gif") || file.endsWith(".png") 
				|| file.endsWith(".tiff") || file.endsWith(".bmp")) {
			imageView.setImageResource(R.drawable.image_icon);
		} else if(file.endsWith(".psd") || file.endsWith(".indd") 
				|| file.endsWith(".ppj") || file.endsWith(".ai")) {
			imageView.setImageResource(R.drawable.adobe_icon);
		}
		else if(position > dirIndex) {
			imageView.setImageResource(R.drawable.file_icon);
		} 
	}
}