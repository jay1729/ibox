package com.teliapp.ibox;

public class EnumerateException extends Exception {

	
	private static final long serialVersionUID = 1L;

	public EnumerateException() {
	}

	public EnumerateException(String msg) {
		super(msg);
	}

}
