package com.teliapp.ibox;

import android.app.Activity;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnBufferingUpdateListener;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;

import com.teliapp.iview.R;

public class VideoActivity extends Activity implements MediaPlayer.OnPreparedListener, SurfaceHolder.Callback, OnCompletionListener, OnBufferingUpdateListener, OnErrorListener {

	private static final String TAG = "VideoViewDemo";
	private MediaPlayer mp;
	private static String PATH;
	private static String NAME;
	private static String IP;
	private static String PORT;
	private SurfaceHolder holder;
	private static SurfaceView VideoPlayer;
	String Url;
	
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		PATH = getIntent().getExtras().getString("PathName");
		Log.e(TAG, PATH);
		NAME = getIntent().getExtras().getString("FileName");
		Log.e(TAG, NAME);
		IP = getIntent().getExtras().getString("IPName");
		Log.e(TAG, IP);
		PORT = getIntent().getStringExtra("PortNo");
		Log.e(TAG, PORT);
		
		
		Log.e(TAG, "Hello World. I shoudl be created");
		
        String file = "";
        if(NAME.contains("mp4") || NAME.contains("m4v") || NAME.contains("mov")){
        	file = "video.m4v";
        }else if(NAME.contains("m4a") || NAME.contains("mp3") || NAME.contains("AIFF")||
        		NAME.contains("wma")){
        	file = "audio.mp3";
        }
        
        Url = "http://" + IP + ":" + "42923" + "/" + file + "/";
        setContentView(R.layout.activity_video);
        VideoPlayer = (SurfaceView) findViewById(R.id.surfaceView1);
        
        holder = VideoPlayer.getHolder();
        holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        holder.addCallback(this);
    }

	private void playVideo() {
		try {

			mp = new MediaPlayer();
			mp.setAudioStreamType(AudioManager.STREAM_MUSIC);
			mp.setScreenOnWhilePlaying(true);

			mp.setDisplay(VideoPlayer.getHolder());
			
			final String url = Url;
			
			mp.setDataSource(url);
			
			Log.e(TAG, "not url");
			Log.e(TAG, url);
			mp.prepare();
			
			int width = mp.getVideoWidth();
			int height = mp.getVideoHeight();
			@SuppressWarnings("deprecation")
			int screen = getWindowManager().getDefaultDisplay().getWidth();
			android.view.ViewGroup.LayoutParams lp = VideoPlayer.getLayoutParams();
		    lp.width = screen;
		    lp.height = (int) (((float)height / (float)width) * (float)screen);
		    VideoPlayer.setLayoutParams(lp);
		     
			Log.e(TAG, "not prepareasync");
			Log.e(TAG, "not prepare");
			mp.start();
 
		} catch (Exception e) {
			Log.e(TAG, "error: " + e.getMessage(), e);
			Log.e(TAG, "THIS IS AN ERROR");
			return;
		}
	}
	public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2, int arg3) {
		Log.d(TAG, "surfaceChanged called");
	}

	public void surfaceCreated(SurfaceHolder arg0) {
		playVideo();
		Log.e(TAG, "surfaceCreated called");
	}

	public void surfaceDestroyed(SurfaceHolder arg0) {
		Log.e(TAG, "surfaceDestroyed called");
	}

	public void onPrepared(MediaPlayer mp) {  
		Log.d(TAG, "onPrepared called");
	}

	public boolean onError(MediaPlayer mediaPlayer, int what, int extra) {
		Log.e(TAG, "onError---> what:" + what + "   extra:" + extra);
		return true;
	}

	@Override
	public void onBufferingUpdate(MediaPlayer arg0, int arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onCompletion(MediaPlayer mp) {
		// TODO Auto-generated method stub
		
	}
}