package com.teliapp.ibox;
 
import java.util.List;

import com.teliapp.iview.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
 
public class DriveArrayAdapter extends ArrayAdapter<String> {
	private final Context context;
	private final List<String> values;
 
	public DriveArrayAdapter(Context context, List<String> values) {
				
		super(context, R.layout.list_view, values);
		this.context = context;
		this.values = values;
	}
 
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context
			.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
 
		View rowView = inflater.inflate(R.layout.list_view, parent, false);
		TextView textView = (TextView) rowView.findViewById(R.id.label);
		ImageView imageView = (ImageView) rowView.findViewById(R.id.logo);
		textView.setText(values.get(position));
		imageView.setImageResource(R.drawable.drive_icon);
 
		return rowView;
	}
}