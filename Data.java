package com.teliapp.ibox;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class Data {
	
	public static final String IP_ADDRESS = "IP address";
	public static final String NAME = "Name";
	public static final String PORT = "Port";
	public static final String START_UP = "Startup";
	
	public static void saveData(Context con, String variable, String data)
	{
	    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(con);
	    prefs.edit().putString(variable, data).commit();
	}
	
	public static String getData(Context con, String variable, String defaultValue)
	{
	    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(con);
	    String data = prefs.getString(variable, defaultValue);

	    return data;
	}
}
