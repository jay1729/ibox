package com.teliapp.ibox;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.teliapp.iview.R;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InterruptedIOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.StringTokenizer;

import javax.xml.crypto.Data;

import static com.sun.org.apache.xalan.internal.xsltc.compiler.sym.error;

public class MainActivity extends SherlockActivity {
 
	public static final int ERROR_HANDLE = 20;
	public static final int APP_HANDLE = 21;
	public static final String ERROR_MSG = "Error message";
	public static final String MSG_TAG = "Message Tag";
	public static final String EXIT_APP = "Exit app";
	public static final String SETTINGS = null;
	public static final int ADD_COMP = 30;
	public static final String FEEDBACK_EMAIL = "iboxfeedback@teliapp.com";
	public static final String HELP_EMAIL = "iboxtechsupport@teliapp.com";
	
	public static final int CONNECT_ERROR = 5;
	public static final int DOWNLOAD_ERROR = 6;
	public static final int FILE_OVERWRITE = 7;
	public static final int DATE_ERROR = 8;
	public static final int DRIVE_ERROR = 50;
	public static final int FILE_OPEN_ERROR = 22;
	
	public static final int DISK_ERROR = 9;

	final String TAG = "MainActivity";
	final int PORT = 9050;
	final String SD_ROOT = "iBox/";
	final String DELIM = "::";
	final String LOAD_DIR = "Loading directory...";
	final String PREV_DIR = "Loading previous directory...";
	final String REFRESH = "Reloading current directory...";

	final String ENUM_DRVS = "enumeratedrives".toUpperCase();
	final String ENUM_DIR = "enumeratedirectories".toUpperCase();
	final String ENUM_FILE = "enumeratefiles".toUpperCase();
	final String GET_FILE = "getfile".toUpperCase();
	final String GET_MOD_DATE = "getlastmodificationdate".toUpperCase();
	final String STREAM_FILE = "streamfile".toUpperCase();

	final String NDF = "NO_DIRECTORIES_FOUND";
	final String NFF = "NO_FILES_FOUND";
	final String NDRF = "NO_DRIVES_FOUND";
	final String ERROR = "ERROR";
	
	private final File sdCard = Environment.getExternalStorageDirectory();
	
	/*
	 * Objects for connection and communication
	 * with the remote host
	 */
	private Socket requestSocket;
	private InputStream in;
 	private PrintWriter out;
 	
 	private StringTokenizer st;
 	private String computerName;
 	private String computerIP;
 	private int portNo;
 	private int dirIndex; // point in the list that is the last directory
 	private String fileToDownload;
 	private boolean errorState;
 	private boolean exitState;
 	private boolean backPress;
 	private boolean downloadCancelled;
 	private int itemClicked;
 	
 	private DownloadTask dtask;
 	private LoadingTask ltask;
 	
 	private ListView directory;
 	private ActionBar actionbar;
 	private ProgressDialog downloadProgress;
 	private ProgressDialog loadingProgress;
 	private TextView noFilesAndDirs;
 	
 	private ArrayList<String> fileList;
 	private ArrayList<String> drivesList;
 	
 	/* 
 	 * a list of all of the current directory/file's parents 
 	 */
 	private ArrayList<String> directoryStack;
 		
	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        downloadCancelled = false;
        itemClicked = 0;
        
        noFilesAndDirs = (TextView) findViewById(R.id.textView1);
        
        computerName = Data.getData(getApplicationContext(), Data.NAME, null);
        computerIP = Data.getData(getApplicationContext(), Data.IP_ADDRESS, null);
        String tempPort = Data.getData(getApplicationContext(), Data.PORT, null);
        actionbar = getSupportActionBar();
        backPress = false;
        
        if(computerName == null && computerIP == null && tempPort == null) {
        	startSettings();
        } else {
        	 
        	portNo = Integer.parseInt(tempPort);
        	
	        actionbar.setTitle(computerName);
	        actionbar.setSubtitle("Hard Drives");
	        actionbar.setHomeButtonEnabled(true);
	        actionbar.setDisplayHomeAsUpEnabled(true);
	  
	        int SDK_INT = android.os.Build.VERSION.SDK_INT;
	        
	        /* Turn strict mode off if the device is running version 11 (Honeycomb) or above */
	        if(SDK_INT >= 11) { 
			    StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
			    StrictMode.setThreadPolicy(policy);
	        }

	        directory = (ListView) findViewById(R.id.listView1);
	        directoryStack = new ArrayList<String>();
	       
	        dirIndex = 0;
	      
	        loadDirectory(LOAD_DIR);
	        
	        setListeners();   
        }
    }
    /*
     * Set the listeners of the activity
     */
    public void setListeners() {    	
    	
    	directory.setClickable(true);
    	directory.setOnItemClickListener(new OnItemClickListener() {

    	@Override
    	public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
    		try {   		
    			
    			if(itemClicked <= 1) {
	    			itemClicked++;
    				exitState = false;

	    			Log.i(TAG, "dirIndex: " + dirIndex + ", position: " + position);
	    			  
	    			if(directoryStack.size() == 0) {
	    				/* add the selected drive to the directory stack */
	    				directoryStack.add(new String(drivesList.get(position)));
	    				  
	    				loadDirectory(LOAD_DIR);
	    			} else {  
		    			if(position <= dirIndex) {
		    				directoryStack.add(fileList.get(position));
			    			loadDirectory(LOAD_DIR);
		    			} else {
		    				String selected = fileList.get(position);
		    				 
		    				if(selected.contains("mp4") || selected.contains("m4v") || selected.contains("mov") ||
		    						selected.contains("m4a") || selected.contains("aiff") || selected.contains("wma")){
		    					Log.e(TAG, "LOLOLOL");
		    					/*VideoActivity videod = new VideoActivity();
		    					Context context = getApplicationContext();
		    					Intent i = new Intent(context, videod.getClass());
		    					i.putExtra("PathName", generatePathName());
		    					i.putExtra("FileName", fileList.get(position));
		    					i.putExtra("IPName", computerIP);
		    					i.putExtra("PortNo", String.valueOf(portNo));*/
		    					fileToDownload = new String(fileList.get(position));
		    					Log.e(TAG, fileToDownload);
		    					DownloadTask dtsk = new DownloadTask();
		    					dtsk.execute(null, null, null);
		    					Log.e(TAG, "execute doesnt stop");
		    					displayMessageDialog("This is Bad", CONNECT_ERROR);
		    					//startActivity(i);
		    					Log.e(TAG, "made new activity");
		    				}else{
		    					checkIfFileExists(fileList.get(position));
		    				}
		    			}
	    			}
	    			  
	    			actionbar.setSubtitle(generatePathName());
	    			itemClicked = 0;
    			}
    		} catch (IOException e) {
				displayMessageDialog(e.getMessage(), CONNECT_ERROR);
				e.printStackTrace();
			} 
    	  }
    	});
    }
    /*
     * return a list of drives from the remote server
     */
    public ArrayList<String> enumerateDrives() throws IOException {
    	
    	sendMessage(ENUM_DRVS);
 
    	Log.i(TAG, "Waiting for drives");
    	
    	String drives = receiveMessage();
    	
    	if(drives.equals(ERROR)) {
    		return null;
    	}
    	
    	Log.i(TAG, "Drives: " + drives);
    	
    	st = new StringTokenizer(drives, ",");
    	ArrayList<String> driveList = new ArrayList<String>(); 
     	
    	while(st.hasMoreTokens()) {
    		driveList.add(st.nextToken());
    	}        	
    	
		closeStreams();
    	
    	return driveList;
    }
    
    /*
     * return a list of directories from the remote server
     */
    public ArrayList<String> enumerateDirs() throws IOException, NullPointerException, EnumerateException {
    	
    	sendMessage(ENUM_DIR + DELIM + generatePathName());
    
    	Log.i(TAG, "Waiting for directories");
    	
    	String dirs = receiveMessage();
    	
    	if(dirs.equals(NDF)) {
    		return new ArrayList<String>();
    	} else if (dirs.equals(ERROR)) {
    		throw new EnumerateException("The directory could not be opened.");
    	} else {

	    	Log.i(TAG, "DIRS: " + dirs);
	    	
	    	st = new StringTokenizer(dirs, ":");
	    	ArrayList<String> dirList = new ArrayList<String>(); 
	     	
	    	while(st.hasMoreTokens()) {
	    		dirList.add(st.nextToken());
	    	}
	    	
	    	dirIndex = dirList.size() - 1;
	    	
	    	closeStreams();
	    	
			return dirList;
    	}
    }
    
    /*
     * return a list of files from the remote server
     */
    public ArrayList<String> enumerateFiles() throws IOException, NullPointerException {
    	
    	initializeStreams();
    	
    	sendMessage(ENUM_FILE + DELIM + generatePathName());
    	
    	Log.i(TAG, "Waiting for files");
    	String files = receiveMessage();
    	
    	if(files.equals(NFF) || files.equals(ERROR)) {
    		closeStreams();
    		return new ArrayList<String>(); 
    	} else {
    	    	
	    	Log.i(TAG, "Files: " + files); 
	    	
	    	st = new StringTokenizer(files, ":");
	    	ArrayList<String> filesList = new ArrayList<String>();
	    	
	    	while(st.hasMoreTokens()) {
	    		filesList.add(st.nextToken());
	    	}
	    	closeStreams();
	    	
	    	return filesList;
    	} 
    } 
    
    public void checkIfFileExists(String filename) throws IOException {
		fileToDownload = new String(filename);
	
		
		if(canSaveToSDCard()) {
		
			File checkDir = new File(sdCard.getAbsolutePath(), generateSDCardPathName());
	    	
	    	if(!checkDir.exists()) {
				checkDir.mkdirs();
			}
			checkDate();
		}
    }
    
    @SuppressWarnings("deprecation")
	public void checkDate() throws IOException {
		File f = new File(sdCard.getAbsolutePath(), generateSDCardPathName() + fileToDownload);
		
		initializeStreams();
		
		if(!f.exists()) {
			getFile();
		} else {
		
			long modDate = f.lastModified();
			sendMessage(GET_MOD_DATE + DELIM + generatePathName() + fileToDownload);
			
			String dateModified = receiveMessage();
			
			if(dateModified.equals(ERROR)) {
				displayMessageDialog("Failed to check the date of the file.", DATE_ERROR);
				errorState = true;
				return;
			}
			
			Date localFileModified = new Date(modDate);
			Date remoteFileModified = new Date(dateModified);
			
			SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss");
			
			String localDate = sdf.format(localFileModified);
			localFileModified = new Date(localDate);
				
			Log.i(TAG, "local: " + localFileModified.toString());
			Log.i(TAG, "remote: " + remoteFileModified.toString());
			
			if(localFileModified.after(remoteFileModified)) {
				displayMessageDialog("A more recent version exists on your device's storage. Overwrite?"
						, FILE_OVERWRITE);
			} else {
				getFile();
			}
		}
    }

    public void getFile() throws IOException {
		    	
		dtask = new DownloadTask();
		
		dtask.execute(null, null, null);
    	
		downloadProgress = ProgressDialog.show(MainActivity.this, "",
				"Downloading file. Please wait...", false);
		
		showToast("Leaving the app may cancel the download");
		
		/* cancel download if back button is pressed */
		downloadProgress.setCancelable(true);
		downloadProgress.setCanceledOnTouchOutside(true);
		downloadProgress.setOnCancelListener(new OnCancelListener() {
		
		    public void onCancel(DialogInterface dialog) {
		    	dtask.cancel(true);
		    	downloadCancelled = true;
		    	closeStreams();
		    	downloadProgress.dismiss();
		    	
		    	File f = new File(sdCard.getAbsolutePath(), generateSDCardPathName() + fileToDownload);
		    	f.delete();
		    }
		});	
    }		
        
	/*
	 * populate the ListView with directories and files
	 */
	public void listFilesAndDirs() {
		FileArrayAdapter adapter = new FileArrayAdapter(this, fileList, dirIndex);
		directory.setAdapter(adapter);
	}
	
	/*
	 * Initial loading of the drives 
	 */
    public void loadDrives() throws IOException {
    	
		
		drivesList = new ArrayList<String>();
		drivesList = enumerateDrives();	
    }
	
	public void loadDirectory(String msg) {

		noFilesAndDirs.setText("");
		
    	loadingProgress = ProgressDialog.show(MainActivity.this, "",
    			msg, false);
    	
    	ltask = null;
    	ltask = new LoadingTask();
    	
    	ltask.execute(null, null, null);

    	loadingProgress.setCancelable(false);
    	loadingProgress.setCanceledOnTouchOutside(false);
	}
	
	/*
	 * establishes input and output streams
	 */
	public void initializeStreams() throws SocketTimeoutException, IOException {
		try {
			
			Log.i(TAG, "Attempting to connect to " + computerIP + " at port " + portNo);
			
			requestSocket = new Socket();
			requestSocket.connect(new InetSocketAddress(computerIP, portNo), 10000);

			Log.i(TAG, "Connected to " + computerIP + " at port " + portNo);
			
			out = new PrintWriter(requestSocket.getOutputStream());

			in = requestSocket.getInputStream();
		
			errorState = false;
		} catch (SocketTimeoutException e) {
			throw e;
		} catch (IOException e) {
			throw e;
		}
	}
	
	public void closeStreams() {
		try {
			in.close();
			out.close();
			requestSocket.close();
		} catch (IOException e) {
				displayMessageDialog(e.getMessage(), CONNECT_ERROR);
				e.printStackTrace();
		}
	}
	
	public void sendMessage(String msg) {
		
		out.print(msg);
		out.flush();
		
		Log.i(TAG, "Message sent: " + msg);
	}

	public String receiveMessage() throws InterruptedIOException, IOException {

		int read;
		
		StringBuffer sb = new StringBuffer();
		
		requestSocket.setSoTimeout(100000);
		
		while((read = in.read()) != -1) {
			sb.append(Character.toChars(read));
		}
		
		requestSocket.setSoTimeout(0);

		String msg = sb.toString();
			
		return msg;
	}

	public void displayMessageDialog(String msg, int flag) {

		Intent retryConn = new Intent(MainActivity.this, RetryActivity.class);
		
		Bundle bundle = new Bundle();
		bundle.putString(ERROR_MSG, msg);
		bundle.putInt(MSG_TAG, flag);
		
		retryConn.putExtras(bundle);
		
		if(flag == CONNECT_ERROR || flag == DOWNLOAD_ERROR) {
			startActivityForResult(retryConn, ERROR_HANDLE);
		} else if(flag == FILE_OPEN_ERROR) {
			startActivityForResult(retryConn, FILE_OPEN_ERROR);
		} else if(flag == DRIVE_ERROR) { 
			startActivityForResult(retryConn, DRIVE_ERROR);
		} else if (flag == DISK_ERROR) {
			startActivityForResult(retryConn, DISK_ERROR);
		} else {
			startActivityForResult(retryConn, APP_HANDLE);
		}
	}
	
	/*
	 * generates the path of the file to send to the server
	 * It is based on the file's parent directory structure
	 * which is in the "parents" ArrayList
	 */
	public String generatePathName() {
	
		StringBuffer sb = new StringBuffer(directoryStack.get(0));
		
		for(int i=1; i<directoryStack.size(); i++) {
			sb.append(directoryStack.get(i)+ "\\");
		}
		
		return sb.toString();
	}
	
	public String generateSDCardPathName() {
		
		StringBuffer sb = new StringBuffer(SD_ROOT);
		
		for(int i=1; i<directoryStack.size(); i++) {
			sb.append(directoryStack.get(i)+ "/");
		}
		
		Log.i(TAG, sb.toString());
		
		return sb.toString();
	}

	public void showToast(String msg) {
		Context context = getApplicationContext();
		CharSequence text = msg;
		int duration = Toast.LENGTH_SHORT;

		Toast toast = Toast.makeText(context, text, duration);
		toast.show();
	}
	
	public void startSettings() {
    	Bundle bundle = new Bundle();
    	bundle.putString(Data.NAME, computerName);
    	bundle.putString(Data.IP_ADDRESS, computerIP);
    	bundle.putInt(Data.PORT, portNo);
    	bundle.putBoolean(SettingsActivity.BACKPRESSED, backPress);
    	Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
    	intent.putExtras(bundle);
    	startActivityForResult(intent, ADD_COMP);
	}
	
	public void closeDialogIfOpen() {
		if(loadingProgress != null) {
			if(loadingProgress.isShowing()) {
				loadingProgress.dismiss();
			}
		}
	}
	
	public void quitApp() {
		Process.killProcess(Process.myPid());
	}
	
	/**
	 * Checks the current status of the SD Card and
	 * displays the appropriate message
	 * @return
	 */
	private boolean canSaveToSDCard()
    {
       boolean cardstate = true; 
 
        String state = Environment.getExternalStorageState();
 
           if (Environment.MEDIA_BAD_REMOVAL.equals(state)) {
        	   cardstate = false;
               displayMessageDialog("SD Card was removed before it could be unmounted.", DISK_ERROR);
           }
           else if (Environment.MEDIA_CHECKING.equals(state)) {
        	   cardstate = false;
               displayMessageDialog("SD Card is currently being disk-checked.", DISK_ERROR);
           }
           else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
        	   cardstate = false;
               displayMessageDialog("SD Card currently has read-only access.", DISK_ERROR);
           }
           else if (Environment.MEDIA_NOFS.equals(state)) {
        	   cardstate = false;
               displayMessageDialog("Unsupported file system on SD Card.", DISK_ERROR);
           }
           else if (Environment.MEDIA_REMOVED.equals(state)) {
        	   cardstate = false;
               displayMessageDialog("SD Card has not been detected on this device.", DISK_ERROR);
           }
           else if (Environment.MEDIA_SHARED.equals(state)) {
        	   cardstate = false;
               displayMessageDialog("SD Card is inaccessible because your device is currently " + 
            		   "being used as USB storage.", DISK_ERROR);
           }
           else if (Environment.MEDIA_UNMOUNTABLE.equals(state)) {
        	   cardstate = false;
               displayMessageDialog("SD Card cannot be mounted.", DISK_ERROR);
           }
           else if (Environment.MEDIA_UNMOUNTED.equals(state)) {
        	   cardstate = false;
               displayMessageDialog("SD Card is not currently mounted.", DISK_ERROR);
           }
           
           return cardstate;
    }
	 
	@SuppressLint("UseValueOf")
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
		
		Bundle bundle = new Bundle();
		
		try {
			bundle = intent.getExtras();
		} catch(NullPointerException e) {
			recreate();
		}
		
		if(bundle == null) {
			Log.i(TAG, "bundle is null");
			return;
		}
		
		switch(requestCode) {
			case FILE_OPEN_ERROR: 
				if(downloadProgress.isShowing()) {
					downloadProgress.dismiss();
				}
				
				break;
			
			case DRIVE_ERROR:
				closeDialogIfOpen();
			case ERROR_HANDLE:
				if(resultCode == RESULT_CANCELED) {
					
					closeDialogIfOpen();

					errorState = false;
					
					boolean exitApp = bundle.getBoolean(EXIT_APP);
					boolean settings = bundle.getBoolean(SETTINGS);
					if(settings) {
						startSettings();
					} else if(exitApp || directoryStack.size() == 0) {
						quitApp();
					}
				} else if(resultCode == RESULT_OK) {
					closeDialogIfOpen();
					
					loadDirectory(LOAD_DIR);
				}
				break;
			case APP_HANDLE:
				if(resultCode == RESULT_CANCELED) {
					closeDialogIfOpen();
				} else if(resultCode == RESULT_OK) {

					try {
						initializeStreams();
						getFile();
					} catch (SocketTimeoutException e) {
						displayMessageDialog("The connection to " + computerIP + " has timed out."
								, CONNECT_ERROR);
						errorState = true;
					}
					catch (IOException e) {
						displayMessageDialog(e.getMessage(), CONNECT_ERROR);
					}
				} 
				break;
			case ADD_COMP:
				
				if(resultCode == RESULT_OK) {
				
					computerName = Data.getData(getApplicationContext(), Data.NAME, null);
					computerIP = Data.getData(getApplicationContext(), Data.IP_ADDRESS, null);
					String tempPort = Data.getData(getApplicationContext(), Data.PORT, null);
					
					if(computerName == null && computerIP == null && tempPort == null) {
						
						computerName = bundle.getString(Data.NAME);
						computerIP = bundle.getString(Data.IP_ADDRESS);
						portNo = bundle.getInt(Data.PORT);
						
						Data.saveData(getApplicationContext(), Data.NAME, computerName);
						Data.saveData(getApplicationContext(), Data.IP_ADDRESS, computerIP);
						Data.saveData(getApplicationContext(), Data.PORT, new Integer(portNo).toString());
						
						startActivity(new Intent(MainActivity.this, MainActivity.class));
						finish();
					} else {
						
						computerName = bundle.getString(Data.NAME);
						computerIP = bundle.getString(Data.IP_ADDRESS);
						portNo = bundle.getInt(Data.PORT);
						
						Data.saveData(getApplicationContext(), Data.NAME, computerName);
						Data.saveData(getApplicationContext(), Data.IP_ADDRESS, computerIP);
						Data.saveData(getApplicationContext(), Data.PORT, new Integer(portNo).toString());
						
						closeDialogIfOpen();
						
						actionbar.setTitle(computerName);

						loadDirectory(LOAD_DIR);
									
					}
				} else {
					if(directoryStack.size() == 0) {
						quitApp();
					}
				}
		}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    MenuInflater inflater = getSupportMenuInflater();
	    inflater.inflate(R.layout.menu_layout, menu);
	    return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		final Intent emailIntent = new Intent(Intent.ACTION_SEND);
		emailIntent.setType("plain/text");
		
	    switch (item.getItemId()) {
	        case android.R.id.home:
	        	onBackPressed();
	        	
	        	return true;
	        case R.id.menu_refresh:

	        	loadDirectory(REFRESH);
	        	return true;
	        case R.id.menu_settings:
	        	startSettings();
	        	return true;
	        case R.id.menu_about_app:
	        	startActivity(new Intent(MainActivity.this, AboutAppActivity.class));
	        	return true;
	        case R.id.menu_about_teliapp:
	        	startActivity(new Intent(MainActivity.this, AboutTeliAppActivity.class));
	        	return true;
	        case R.id.menu_feedback:
            	emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, 
            			new String[]{FEEDBACK_EMAIL});
            	emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Feedback");
            	startActivity(Intent.createChooser(emailIntent, "Send mail..."));
	        	return true;
	        case R.id.menu_help:
            	emailIntent.setType("plain/text");
            	emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, 
            			new String[]{HELP_EMAIL});
            	emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Help");
            	startActivity(Intent.createChooser(emailIntent, "Send mail..."));
	        	return true;
	        case R.id.menu_credits:
	        	startActivity(new Intent(MainActivity.this, CreditsActivity.class));
	        	return true;
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}
	
	/*
	 * (non-Javadoc)
	 * @see android.app.Activity#onBackPressed()
	 */
	@Override
	public void onBackPressed() {
		
		if(directoryStack.size() == 0) {
			
			if(exitState) {
				quitApp();
			} else {
				showToast("Press <back> again to exit");
				exitState = true;
			}
		} else if(directoryStack.size() == 1) {
			actionbar.setSubtitle("Hard Drives");
			directoryStack.remove(0);

			DriveArrayAdapter daa = new DriveArrayAdapter(getApplicationContext(), drivesList);
		    directory.setAdapter(daa);
		    closeStreams();
		    
		} else {
			directoryStack.remove(directoryStack.size() - 1);
			actionbar.setSubtitle(generatePathName());
			loadDirectory(PREV_DIR);
		}
	}
	
	/*
	 * Below are the background tasks that will handle loading a directory
	 * and downloading a file
	 */
	
	class LoadingTask extends AsyncTask<Void, Void, Void> {
		
		@Override
		protected Void doInBackground(Void... arg0) {
			
			try {
				initializeStreams();
				
				if(directoryStack.size() == 0) {
					loadDrives();
				} else {

					fileList = enumerateDirs();
					
					if(fileList.size() > 0) {
						dirIndex = fileList.size() - 1;
					} else 
						dirIndex = -1;
					
					fileList.addAll(enumerateFiles());
					
				} 
			} catch (SocketTimeoutException e) {
				displayMessageDialog("The connection to " + computerIP + " has timed out."
						, CONNECT_ERROR);
				errorState = true;
			} catch (InterruptedIOException e) {
				displayMessageDialog("The connection to " + computerIP + " has timed out."
						, CONNECT_ERROR);
				errorState = true;
			} catch(IOException e) {
				errorState = true;
				displayMessageDialog(e.getMessage(), CONNECT_ERROR);
			} catch (EnumerateException e) {
				directoryStack.remove(directoryStack.size() - 1);
				displayMessageDialog(e.getMessage(), DRIVE_ERROR);
			}
			
			return null;
		}
		
		 protected void onPostExecute(Void arg0) {
			 
			 if(!errorState) {
				 if(directoryStack.size() == 0) {
					 
					 if(drivesList.size() == 0) {
						 showToast("There are no drives on this computer");
					 }

				    DriveArrayAdapter daa = new DriveArrayAdapter(getApplicationContext(), drivesList);
				    directory.setAdapter(daa);
				 } else {
					 if(fileList.size() == 0) {
						 noFilesAndDirs.setText("Empty Directory");
					 }
					 
					 fileList.remove("NFF");
					 fileList.remove("NDF");
					 
					 listFilesAndDirs();

				 }
					if(loadingProgress.isShowing()) {
						loadingProgress.dismiss();
					}
			 }
			 errorState = false;
		 }
	} 
	boolean video;
	class DownloadTask extends AsyncTask<Void, Void, Void> {

		@Override
		protected Void doInBackground(Void... arg0) {
	    	try {
	    		
	    		String extention = fileToDownload.substring(1);
	    		Log.e("extension", extention);
		    	if(extention.contains("mp4") || extention.contains("m4v") || extention.contains("mov") ||
						extention.contains("m4a") || extention.contains("aiff") || extention.contains("wma")){
		    		sendMessage(STREAM_FILE + DELIM + generatePathName() + fileToDownload);
		    		Log.e(TAG, "We are beginning to download");
		    		video = true;
		    		Log.e("something","STREAMFILE" + DELIM + generatePathName() + fileToDownload);
	    			
	    		}else{
	    			video = false;
		    		File file = new File(sdCard.getAbsolutePath(), generateSDCardPathName() + fileToDownload);
		    		if(file.exists()) {
		    			boolean deleted = file.delete();
		    			Log.i(TAG, "File was deleted: " + deleted);
		    		}
		    		
			    	requestSocket.setSoTimeout(100000);
		    	
	    			sendMessage(GET_FILE + DELIM + generatePathName() + fileToDownload);
	    			
	    		
			    	Log.i(TAG, "Save to: " + generateSDCardPathName() + fileToDownload);
			    	
					OutputStream os = new BufferedOutputStream(new FileOutputStream(
							new File(sdCard.getAbsolutePath(), generateSDCardPathName() + fileToDownload)));
			    	InputStream is = requestSocket.getInputStream();
			    	
			    	byte [] buffer = new byte[96600];
			    	int numRead;
			    	
			    	int count = 0;
			    	
			    	/* 
			    	 * read the file's bytes from the input stream, 
			    	 * and write them to the file output stream
			    	 */
			    	while((numRead = is.read(buffer)) != -1) {
			    		Log.i(TAG, "numRead: " + numRead + ", " + count++);
			    		os.write(buffer, 0, numRead);
			    	} 
			    	
			    	requestSocket.setSoTimeout(0);
			    	
			    	os.flush();
	    		}
	    	} catch(InterruptedIOException e) {
	    		errorState = true;
	    		displayMessageDialog(e.getMessage(), DOWNLOAD_ERROR);
	    	} catch(IOException e) {
	    		if(!downloadCancelled) {
		    		errorState = true;
		    		e.printStackTrace();
		    		displayMessageDialog(e.getMessage(), DOWNLOAD_ERROR);
	    		} else {
	    			downloadCancelled = false;
	    		}
	    	}
			return null;
		}
		
		protected void onPostExecute(Void arg0) {
	       if(video == true){
	    	   
	    	   
	       }else{
			if(!errorState) {
				if(downloadProgress.isShowing()) {
					downloadProgress.dismiss();
				}
			 	closeStreams();
			    
				/* load the downloaded file, and open it in the appropriate
				 * android app based on its extension
			 	 */
				File download = new File(sdCard.getAbsolutePath(), generateSDCardPathName() + fileToDownload);
			
				Intent intent = new Intent(Intent.ACTION_VIEW);
				String extension 
					= MimeTypeMap.getFileExtensionFromUrl(Uri.fromFile(download).toString());
				String mimetype 
					= MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
				intent.setDataAndType(Uri.fromFile(download),mimetype);
				
				try {
					startActivity(intent);
				} catch(Exception e) {
					download.delete();
					errorState = false;
					displayMessageDialog("File could not be opened.", FILE_OPEN_ERROR);
				}
				
			}
			errorState = false;
		}
		}
	}
	
	@Override
	public void onDestroy() {	
		super.onDestroy();
		
		closeStreams();
		quitApp();
	}
}