package com.teliapp.ibox;

import com.teliapp.iview.R;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import android.os.Bundle;
import com.actionbarsherlock.view.MenuItem;

public class CreditsActivity extends SherlockActivity {
    public void onCreate(Bundle savedInstanceState) {
    	
    	super.onCreate(savedInstanceState);
        setContentView(R.layout.credits);
        
        ActionBar ab = getSupportActionBar();
        
        ab.setTitle("Credits");
        ab.setHomeButtonEnabled(true);
        ab.setDisplayHomeAsUpEnabled(true);
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

	    switch (item.getItemId()) {
	        case android.R.id.home:
	        	onBackPressed();
	        	
	        	return true;
	        default:
	        	return false;
    	}
    	
    }

}
