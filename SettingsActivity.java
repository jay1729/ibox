package com.teliapp.ibox;

import com.teliapp.iview.R;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Process;
import android.util.Log;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class SettingsActivity extends SherlockActivity{

	final String TAG = "AddComputerActivity";
	public static final String BACKPRESSED = "Backpressed";
	public static final String QUIT = "quit";
	
	EditText nameField;
	EditText ipField;
	EditText portField;
	Button submit;
	boolean newUser;
	boolean backPress;
	
	/** Called when the activity is first created. */
    @SuppressLint("UseValueOf")
	@Override
    public void onCreate(Bundle savedInstanceState) {
    	
    	super.onCreate(savedInstanceState);
        setContentView(R.layout.addcomputer);
        
        nameField = (EditText) findViewById(R.id.editText1);
        ipField = (EditText) findViewById(R.id.editText2);
        portField = (EditText) findViewById(R.id.editText3);
        submit = (Button) findViewById(R.id.button1);
        
        //ipField.setRawInputType(Configuration.KEYBOARD_QWERTY);
        
        Bundle bundle = getIntent().getExtras();
        String computerName = bundle.getString(Data.NAME);
        String computerIP = bundle.getString(Data.IP_ADDRESS);
        backPress = bundle.getBoolean(BACKPRESSED);
        int portNo = bundle.getInt(Data.PORT);
        
        ActionBar actionbar = getSupportActionBar();
        
        if(computerName == null && computerIP == null) {
        	actionbar.setTitle("Welcome! Add a computer...");
        	newUser = true;
        } else {
        	actionbar.setTitle("Settings");
        	if(!backPress) {
        		actionbar.setTitle("Settings");
        		actionbar.setDisplayHomeAsUpEnabled(true);
        		actionbar.setHomeButtonEnabled(true);
        	}
        	
        	
            nameField.setText(computerName);
            ipField.setText(computerIP);
            portField.setText(new Integer(portNo).toString());
            newUser = false;
        }
         
        submit.setOnClickListener(new OnClickListener() {
        	
        	@Override
        	public void onClick(View v) {
        		Bundle b = new Bundle();

        		Log.i("AddComputerActivity", nameField.getText().toString());
        		Log.i("AddComputerActivity", ipField.getText().toString());
        		
        		String name = nameField.getText().toString();
        		String ip = ipField.getText().toString();
        		
        		try {
        			Integer port = Integer.parseInt(portField.getText().toString());
        			
            		if(!name.equals("") && !ip.equals("") && port != null) {
                		
            			if(!checkIp(ip)) {
            				showToast("Please enter a correctly formatted IP address: [0-255].[0-255].[0-255].[0-255]");
            			} else {
    	        			b.putString(Data.NAME, name);
    	            		b.putString(Data.IP_ADDRESS, ip);
    	            		b.putInt(Data.PORT, port);
    	            		
    	            		Intent intent = new Intent();
    	            		intent.putExtras(b);
    	            		setResult(RESULT_OK, intent);
    	            		
    	            		finish();
            			}
            		} else {
            			showToast("The fields cannot be left empty.");
            		}
        		} catch(NumberFormatException e) {
        			showToast("Invalid port number entered.");
        		}        		
        	}
        });
    }
    
    public void showToast(String msg) {
		Context context = getApplicationContext();
		CharSequence text = msg;
		int duration = Toast.LENGTH_LONG;

		Toast toast = Toast.makeText(context, text, duration);
		toast.show();
    }
    
    public static boolean checkIp (String sip) {
    	
    	String [] parts = sip.split ("\\.");
    	
    	if(parts.length != 4) {
    		return false;
    	}
        
    	for (String s : parts) {
    		int i = Integer.parseInt (s);
            
    		if (i < 0 || i > 255) {
                return false;
            }
        }
        return true;
    } 
    
    @Override
    public void onBackPressed() {
		
    	if(!newUser && !backPress) {
	    	Intent intent = new Intent();
			Bundle b = new Bundle();

			intent.putExtras(b);
			setResult(RESULT_CANCELED, intent);
			
			
			finish();
    	} else if(newUser) {
    	
    		Process.killProcess(Process.myPid());
    		
    		/*
    		Log.i(TAG, "back pressed, new User");
	    	Intent intent = new Intent();
			Bundle b = new Bundle();
			quit = true;
			b.putBoolean("QUIT", quit);

			Log.i(TAG, "back pressed, new User: " + quit);
			intent.putExtras(b);
			setResult(RESULT_CANCELED, intent);
			
			finish();*/
    	}
    }
    
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    
		if(newUser) {
			MenuInflater inflater = getSupportMenuInflater();
			inflater.inflate(R.layout.settings_menu, menu);
		}
	    return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		
		final Intent emailIntent = new Intent(Intent.ACTION_SEND);
		emailIntent.setType("plain/text");
		
	    switch (item.getItemId()) {
	        case android.R.id.home:
	        	onBackPressed();
	        	return true;
	        case R.id.menu_about_app:
	        	startActivity(new Intent(SettingsActivity.this, AboutAppActivity.class));
	        	return true;
	        case R.id.menu_about_teliapp:
	        	startActivity(new Intent(SettingsActivity.this, AboutTeliAppActivity.class));
	        	return true;
	        case R.id.menu_feedback:
            	emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, 
            			new String[]{MainActivity.FEEDBACK_EMAIL});
            	emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Feedback");
            	startActivity(Intent.createChooser(emailIntent, "Send mail..."));
	        	return true;
	        case R.id.menu_help:
            	emailIntent.setType("plain/text");
            	emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, 
            			new String[]{MainActivity.HELP_EMAIL});
            	emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Help");
            	startActivity(Intent.createChooser(emailIntent, "Send mail..."));
	        	return true;
	        case R.id.menu_credits:
	        	startActivity(new Intent(SettingsActivity.this, CreditsActivity.class));
	        	return true;
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}
}
