package com.teliapp.ibox;

import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.teliapp.iview.R;

public class StreamActivity extends SherlockActivity{
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		ActionBar actionbar = getSupportActionBar();
		actionbar.hide();
		
		System.out.println("In StreamActivity");
		
		Bundle bundle = getIntent().getExtras();
		
		String msg = bundle.getString(MainActivity.ERROR_MSG);
		int flag = bundle.getInt(MainActivity.MSG_TAG);
		Builder builder = new Builder(this);
		builder.setMessage(msg);
		
		if(flag == MainActivity.FILE_OPEN_ERROR || flag == MainActivity.DRIVE_ERROR || flag == MainActivity.DISK_ERROR) {
			builder.setTitle("Error");
			builder.setNeutralButton("Okay", 
				new OnClickListener() {
					public void onClick(DialogInterface dialog, int arg1) {
						dialog.dismiss();
						Intent intent = new Intent();
						intent.putExtras(new Bundle());
						setResult(RESULT_OK, intent);
						finish();
					}
				});
		} else if(flag == MainActivity.CONNECT_ERROR || flag == MainActivity.DOWNLOAD_ERROR || flag == MainActivity.DATE_ERROR) {
			builder.setTitle("Error");
			if(flag == MainActivity.CONNECT_ERROR) {
				builder.setNeutralButton("Retry", 
					new OnClickListener() {
						public void onClick(DialogInterface dialog, int arg1) {
							dialog.dismiss();
						    Intent intent = new Intent();
						    intent.putExtras(new Bundle());
						    setResult(RESULT_OK, intent);
						    finish();
						}
					}
				);
			} else if(flag == MainActivity.DOWNLOAD_ERROR) {
				builder.setNeutralButton("Cancel", 
						new OnClickListener() {
							public void onClick(DialogInterface dialog, int arg1) {
								dialog.dismiss();
							    Intent intent = new Intent();
							    intent.putExtras(new Bundle());
							    setResult(RESULT_OK, intent);
							    finish();
							}
						}
					);
			}
			
			if(flag == MainActivity.DATE_ERROR) {
				builder.setNegativeButton("Back",
						new OnClickListener() {
							public void onClick(DialogInterface dialog, int arg1) {
								Intent intent = new Intent();
								Bundle b = new Bundle();
							 	b.putBoolean(MainActivity.EXIT_APP, false);
								intent.putExtras(b);
								setResult(RESULT_CANCELED, intent);
								finish();
							}
						}
					);
			} else {
				builder.setPositiveButton("Settings",
					new OnClickListener() {
						public void onClick(DialogInterface dialog, int arg1) {
						    Intent intent = new Intent();
							Bundle b = new Bundle();
							b.putBoolean(MainActivity.SETTINGS, true);
							intent.putExtras(b);
						    setResult(RESULT_CANCELED, intent);
						    finish();
						}
					}
				);
				builder.setNegativeButton("Exit",
					new OnClickListener() {
						public void onClick(DialogInterface dialog, int arg1) {
							Intent intent = new Intent();
							Bundle b = new Bundle();
							b.putBoolean(MainActivity.EXIT_APP, true);
							intent.putExtras(b);
							setResult(RESULT_CANCELED, intent);
							finish();
						}
					}
				);
			}
		} else {

			builder.setPositiveButton("Yes", 
				new OnClickListener() {
					public void onClick(DialogInterface dialog, int arg1) {
					    Intent intent = new Intent();
					    intent.putExtras(new Bundle());
					    setResult(RESULT_OK, intent);
					    finish();
					}
				}
			);
			builder.setNegativeButton("No", 
				new OnClickListener() {
					public void onClick(DialogInterface dialog, int arg1) {
					    Intent intent = new Intent();
					    intent.putExtras(new Bundle());
					    setResult(RESULT_CANCELED, intent);
					    finish();
					}
				}
			);
		}
		
		builder.setOnCancelListener(new OnCancelListener() {
			
			@Override
			public void onCancel(DialogInterface Dialog) {
				onBackPressed();
			}
		});
		builder.show();
				
		setContentView(R.layout.retry);
	}
	
	@Override
	public void onBackPressed() {
	    Intent intent = new Intent();
	    intent.putExtras(new Bundle());
	    setResult(RESULT_CANCELED, intent);
	    finish();
	}
}
